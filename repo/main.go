package repo

import git "github.com/go-git/go-git/v5"

type Repo struct {
	gitRepo *git.Repository
}
